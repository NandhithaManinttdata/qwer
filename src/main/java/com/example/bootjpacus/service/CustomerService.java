package com.example.bootjpacus.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bootjpacus.dao.CustomerRepository;
import com.example.bootjpacus.model.Customer;

@Service
public class CustomerService 

{
	  @Autowired
	  CustomerRepository repo;
	public void savedetails(Customer cus) {
		repo.save(cus);
		
	}
	
	public void save(Customer cus) {
		repo.save(cus);
		
	}

	public Optional<Customer> findById(int cid) {
		Optional<Customer> cus=repo.findById(cid);
		return cus;
	}

	public List<Customer> findAll() {
		return (List<Customer>) repo.findAll();
	}

	
	public Customer getOne(int cid) {
		Customer cus=repo.getOne(cid);
		return cus;
	}
	public void delete(Customer cus) {
		repo.delete(cus);
		
	}

	

	
	
	
      	
	
}
