package com.example.bootjpacus.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.bootjpacus.model.Customer;
import com.example.bootjpacus.service.CustomerService;

@RestController

public class CustomerController 
{
	@Autowired
	CustomerService service;
	
	 @GetMapping("/healthcheckup")          	

	  public String getString(@RequestBody String init)
	  { 
		 if(init.equals("can we start"))
		 
			{
			 System.out.println("hi this is want u sent us"+init);
		  
		      return "all good to go";
			
			}
		
		 else
		 {
			 return "service doesnt exist";
		 }
	  }

 
	  @GetMapping("/Customers")          	

	  public ResponseEntity getCustomers()
	  { 
		  return new ResponseEntity(service.findAll(), HttpStatus.OK); 
	    }
	  
	  
	  @GetMapping("/Customers/{cid}")

	  public ResponseEntity getCustomer(@PathVariable("cid") int cid)    
	  {
	  	
		 Optional<Customer> customer=service.findById(cid);
		if(customer.isPresent())
		   
		{  return new ResponseEntity(customer, HttpStatus.OK);} //200,404
		
		else
		{
			 return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
	  }

	
	  @PostMapping("/Customers")
	  public ResponseEntity addCustomers(Customer customer)
	  {
	  	
		  service.save(customer);
	  	 
	      return new ResponseEntity(customer, HttpStatus.OK); //201,409(resource  conflict)
	      
	  }

	   @PutMapping("/Customers")
	  public ResponseEntity updateCustomers(@RequestBody Customer customer)
	  { 
              service.save(customer);
	   	      
              return new ResponseEntity(customer, HttpStatus.OK); //200 (OK) or 204 (No Content). 404 (Not Found),405(method not allowed)
	  }

	  @DeleteMapping("/Customers/{cid}")
	  public ResponseEntity deleteCustomer(@PathVariable("cid") int cid)
	  {
		  Customer customer=service.getOne(cid);
		  service.delete(customer);
	      return new ResponseEntity("Customer details got removed", HttpStatus.OK);//200,404,405
	  	
	  }
	  
	  
    }
