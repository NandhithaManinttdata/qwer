package com.example.bootjpacus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootJpaCusApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootJpaCusApplication.class, args);
	}

}
