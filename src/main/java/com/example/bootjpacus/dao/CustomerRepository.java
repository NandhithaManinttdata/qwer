package com.example.bootjpacus.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.bootjpacus.model.Customer;

public interface CustomerRepository  extends JpaRepository<Customer, Integer>

{
	 
	
}
